# Allgemeine Infos zur (mündlichen) Prüfung in 366.090 Mikro- und Nanosystemtechnik 

Die Prüfung fand bei mir (WS19/20) alleine statt, die Atmosphäre ist sehr entspannt.

Prof. Schmid fragt anfangs was einem gut, was einem nicht gut an den Vorträgen gefallen hat. Das ist nicht Teil der Prüfung sondern für ihn als Feedback dafür, welche Vortragende er wieder einladen soll.

Ich hab dann ein paar Dinge aufgezählt, die ich besonders interessant fand bzw. hängen geblieben sind. Als ich einen Sensor konkret benannt hat hat er sich dann auf den fokussiert, gefragt, was das Wirkprinzip davon ist und welche fundamentalen Probleme bzw. Herausforderungen mir bei diesem Sensor einfallen. So wurde ca. 10 min über den Sensor diskutiert, damit war die formelle Prüfung fertig.

Ich empfehle überblicksmäßig über die Folien zu schauen und sich ein, zwei Sachen genauer anzuschauen, die man dann bespricht. Wenn man statt über Sensoren lieber über genauer Waferbonding spricht ist das wahrscheinlich auch kein Problem.